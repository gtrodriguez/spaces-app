import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import AssetsTab from '../../components/assetstab';
import AssetsData from '../../../../mockdata/assetslist';

describe('AssetsTab', () =>{
  Enzyme.configure({ adapter: new Adapter() })
  let assets = AssetsData.items;

  it('renders correctly',() => {
    const component = Enzyme.shallow(<AssetsTab
      assets={assets} />);
    expect(component).toMatchSnapshot();
    expect(component.find("[data-asset-id]").length).toEqual(3);
  });
});