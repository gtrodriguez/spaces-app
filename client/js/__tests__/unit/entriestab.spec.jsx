import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import EntriesTab from '../../components/entriestab';
import EntriesData from '../../../../mockdata/entrieslist';

describe('EntriesTab', () =>{
  Enzyme.configure({ adapter: new Adapter() })
  let entries = EntriesData.items;

  it('renders correctly',() => {
    const component = Enzyme.shallow(<EntriesTab
      entries={entries} />);
    expect(component).toMatchSnapshot();
    expect(component.find("[data-entry-id]").length).toEqual(3);
  });
});