import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import Navigation from '../../components/navigation';
import SpaceData from '../../../../mockdata/spaceslist';

describe('Navigation', () =>{
  Enzyme.configure({ adapter: new Adapter() })
  let spaces = SpaceData.items;

  it('renders correctly',() => {
    const component = Enzyme.shallow(<Navigation
      spaces={spaces} />);
    expect(component).toMatchSnapshot();
    expect(component.find("[data-space-id]").length).toEqual(3);
  });
});
