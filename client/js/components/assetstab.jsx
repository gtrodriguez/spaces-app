import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

class AssetsTab extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sortAttr: null,
    };
  }

  renderContent() {
    if (this.props.assets.length) {
      return (<Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>Title</th>
            <th>Content Type</th>
            <th>File Name</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Last Updated</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.assets.map(asset => (
              <tr key={asset.sys.id} data-asset-id={asset.sys.id}>
                <td>{asset.fields.title}</td>
                <td>{asset.fields.contentType}</td>
                <td>{asset.fields.fieldName}</td>
                <td>{asset.sys.createdBy}</td>
                <td>{asset.sys.updatedBy}</td>
                <td>{new Date(asset.sys.updatedAt).toLocaleString('en-US')}</td>
              </tr>
            ))
          }
        </tbody>
      </Table>);
    }

    return <p><strong>There are no assets available for this space!</strong></p>;
  }

  render() {
    return (<div id="assets-tab-content" className="tab-container">
      {
        this.renderContent()
      }
      </div>
    );
  }
}

AssetsTab.propTypes = {
  assets: PropTypes.arrayOf(PropTypes.shape({
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired,
      fileName: PropTypes.string.isRequired,
      contentType: PropTypes.string.isRequired,
    }),
    sys: PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      createdBy: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
      updatedBy: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
};

export default AssetsTab;
