import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

class EntriesTab extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sortAttr: null,
    };
  }

  renderContent() {
    if (this.props.entries.length) {
      return (<Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>Title</th>
            <th>Summary</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Last Updated</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.entries.map(entry => (
              <tr key={entry.sys.id} data-entry-id={entry.sys.id}>
                <td>{entry.fields.title}</td>
                <td>{entry.fields.summary}</td>
                <td>{entry.sys.createdBy}</td>
                <td>{entry.sys.updatedBy}</td>
                <td>{new Date(entry.sys.updatedAt).toLocaleString('en-US')}</td>
              </tr>
            ))
          }
        </tbody>
      </Table>);
    }

    return (<p><strong>There are no entries available for this space!</strong></p>);
  }

  render() {
    return (<div id="entries-tab-container" className="tab-container">
      {
        this.renderContent()
      }
    </div>);
  }
}

EntriesTab.propTypes = {
  entries: PropTypes.arrayOf(PropTypes.shape({
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired,
      summary: PropTypes.string,
    }),
    sys: PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      createdBy: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
      updatedBy: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
};

export default EntriesTab;
