import React from 'react';
import PropTypes from 'prop-types';
import { LinkContainer } from 'react-router-bootstrap';
import { Nav, NavItem } from 'react-bootstrap';

class Navigation extends React.Component {
  constructor(props) {
    super(props);
  }

  getActiveKey() {
    if (this.props.spaceKey) {
      return this.props.spaces.find(space => space.sys.id === this.props.spaceKey)
    }

    return null;
  }

  render() {
    return (<Nav bsStyle="tabs" stacked activeKey={this.getActiveKey()}>
      {
        this.props.spaces.map((space, index) => (
          <LinkContainer to={`/${space.sys.id}`} key={space.sys.id} eventKey={index}>
            <NavItem
              data-space-id={space.sys.id}
            >
              {space.fields.title}
            </NavItem>
          </LinkContainer>))
      }
    </Nav>);
  }
}

Navigation.defaultProps = {
  spaceKey: null,
};

Navigation.propTypes = {
  spaceKey: PropTypes.string,
  spaces: PropTypes.arrayOf(PropTypes.shape({
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired,
      description: PropTypes.string,
    }).isRequired,
    sys: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
};

export default Navigation;
