import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import Spaces from './pages/spaces';
import NotFound from './pages/notfound';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      spaces: [],
      entries: [],
      assets: [],
      users: [],
      allLoaded: false,
    };

    this.handleUpdateEntries = this.handleUpdateEntries.bind(this);
    this.handleUpdateAssets = this.handleUpdateAssets.bind(this);
  }

  componentDidMount() {
    var promises = []; 

    promises.push(axios.get('/api/users').then(result => {
      this.setState({
        users: result.data.items,
      });
    }));

    // this would eventually take an argument for the user
    promises.push(axios.get('/api/space').then(result => {
      this.setState({
        spaces: result.data.items,
      });

    }));

    axios.all(promises).then(()=>{
      this.setState({allLoaded: true});
    })
  }

  handleUpdateEntries(entries) {
    this.setState({
      entries: entries,
    });
  }

  handleUpdateAssets(assets) {
    this.setState({
      assets: assets,
    });
  }

  render() {
    return (
      <div id="app-container">
        <BrowserRouter basename={'/spaceexplorer'} forceRefresh={false}>
          <div className="app-2">
            <Switch>
              <Route
                exact
                path="/:spaceId?"
                render={({ match }) => (
                  <Spaces
                    spaceId={match.params.spaceId} 
                    users={this.state.users}
                    spaces={this.state.spaces}
                    entries={this.state.entries}
                    assets={this.state.assets}
                    allLoaded={this.state.allLoaded}
                    handleUpdateEntries={this.handleUpdateEntries}
                    handleUpdateAssets={this.handleUpdateAssets}
                  />
                )}
              />
              <Route component={NotFound} />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

render(<App />, document.getElementById('app'));

export default App;
