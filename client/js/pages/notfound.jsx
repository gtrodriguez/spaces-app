import React from 'react';

class NotFound extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (<section>
      <h1>404 Page Not Found</h1>
      <p>Sorry, but the page you were looking for was not found!</p>
    </section>);
  }
}

export default NotFound;
