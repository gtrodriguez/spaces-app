import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { Grid, Row, Col, Tabs, Tab, Jumbotron } from 'react-bootstrap';
import axios from 'axios';
import Navigation from '../components/navigation';
import AssetsTab from '../components/assetstab';
import EntriesTab from '../components/entriestab';

class Spaces extends React.Component {
  constructor(props) {
    super(props);

    this.renderSpaceView = this.renderSpaceView.bind(this);
    this.updateData = this.updateData.bind(this);
  }

  componentDidMount(){
    if (this.props.spaceId) {
      this.updateData(this.props.spaceId);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.spaceId !== nextProps.spaceId) {
      this.updateData(nextProps.spaceId);
    }
  }

  updateData(spaceId) {
    const promises = [];

    promises.push(axios.get(`/api/space/${spaceId}/assets`)
      .then((response) => {
        this.props.handleUpdateAssets(response.data.items);
      }));

    promises.push(axios.get(`/api/space/${spaceId}/entries`)
      .then((response) => {
        this.props.handleUpdateEntries(response.data.items);
      }));

    axios.all(promises).catch((response) => {
      // handle type of error
    });
  }

  renderSpaceView() {
    return (<Tabs defaultActiveKey={1} id="users-data-tabs">
      <Tab eventKey={1} title="Entries">
        <EntriesTab entries={this.props.entries} />
      </Tab>
      <Tab eventKey={2} title="Assets">
        <AssetsTab assets={this.props.assets} />
      </Tab>
    </Tabs>);
  }

  renderLandingContent() {
    return (<Jumbotron id="space-landing-content">
      <h1>Welcome to Space Explorer!</h1>
      <p>This is a simple programming execise. Please select a space to see more.</p>
    </Jumbotron>);
  }

  renderContent() {
    if (this.props.allLoaded) {
      if (this.props.spaces.length){
        return (<Row>
          <Col sm={4} md={3} lg={2}>
            <Navigation spaces={this.props.spaces} />
          </Col>
          <Col sm={8} md={9} lg={10}>
            <Switch>
              <Route
                exact
                path="/:spaceId"
                render={({ match }) => (this.renderSpaceView(match.params.spaceId))} />
              <Route
                render={() => (this.renderLandingContent())} />
            </Switch>
          </Col>
        </Row>);
      }
      return <div><strong>Sorry, no spaces available. Please create one and return.</strong></div>;
    }
    return <div id="loading-data-msg"><strong>Loading Data...</strong></div>;
  }

  render() {
    return (<section id="spaces-page" className="spaces-page">
      <Grid>
        {
          this.renderContent()
        }
      </Grid>
    </section>);
  }
}

Spaces.defaultProps = {
  spaceId: null,
};

Spaces.propTypes = {
  spaceId: PropTypes.string,
  spaces: PropTypes.arrayOf(PropTypes.shape({
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired,
      description: PropTypes.string,
    }).isRequired,
    sys: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  assets: PropTypes.arrayOf(PropTypes.shape({
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired,
      fileName: PropTypes.string.isRequired,
      contentType: PropTypes.string.isRequired,
    }),
    sys: PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      createdBy: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
      updatedBy: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  entries: PropTypes.arrayOf(PropTypes.shape({
    fields: PropTypes.shape({
      title: PropTypes.string.isRequired,
      summary: PropTypes.string,
    }),
    sys: PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      createdBy: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
      updatedBy: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  allLoaded: PropTypes.bool.isRequired,
  handleUpdateEntries: PropTypes.func.isRequired,
  handleUpdateAssets: PropTypes.func.isRequired,
};

export default Spaces;
