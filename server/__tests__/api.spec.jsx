import React from 'react';
import axios from 'axios';
import App from '../app';
const path = require('path');
const lib = path.join(path.dirname(require.resolve('axios')),'lib/adapters/http');
const http = require(lib);
let server;

beforeAll(async () => {
  server = await App();
});

afterAll(done =>{
  server.close(done);
});

test('can get spaces', async () =>{
  const response = await axios.get('http://localhost:3000/api/space', {
    adapter: http
  }).catch((err) => {console.log(err)});

  const spaces = response.data.items;
  expect(spaces.length).toEqual(3);

  let space = spaces[0];

  expect(space.fields.title).toEqual('My First Space');
  expect(space.sys.id).toEqual('yadj1kx9rmg0');
});

test('can get assets', async () =>{
  const response = await axios.get('http://localhost:3000/api/space/yadj1kx9rmg0/assets', {
    adapter: http
  }).catch((err) => {console.log(err)});

  const assets = response.data.items;
  expect(assets.length).toEqual(3);

  let asset = assets[0];

  expect(asset.fields.title).toEqual('Hero Collaboration Partial');
  expect(asset.fields.fileName).toEqual('hero-collaboration-partial.png');
  expect(asset.fields.contentType).toEqual('image/png');
  expect(asset.sys.space).toEqual('yadj1kx9rmg0');
});

test('can get entries', async () =>{
  const response = await axios.get('http://localhost:3000/api/space/yadj1kx9rmg0/entries', {
    adapter: http
  }).catch((err) => {console.log(err)});

  const entries = response.data.items;
  expect(entries.length).toEqual(3);

  let entry = entries[0];

  expect(entry.fields.title).toEqual('Hello, World!');
  expect(entry.sys.space).toEqual('yadj1kx9rmg0');
  expect(entry.sys.createdBy).toEqual("4FLrUHftHW3v2BLi9fzfjU");
  expect(entry.sys.createdAt).toEqual("2015-05-18T11:29:46.809Z");
  expect(entry.sys.updatedBy).toEqual("4FLrUHftHW3v2BLi9fzfjU");
  expect(entry.sys.updatedAt).toEqual("2015-05-18T11:29:46.809Z");
});
