const express = require('express');
const app = express();
const http = require('http').Server(app);
const path = require('path');
const router = require('./routes/index');

function startServer() {
  app.use('/public', express.static(path.join(__dirname, '../public')));
  app.set('view engine', 'ejs');
  app.use('/', router);

  let gameInstance = null;
  let timeStamp = null;
  let currentPlayer = null;

  return new Promise(resolve => {
      const server = http.listen(process.env.PORT || 3000, () => {
        resolve(server);
      })
  });
}

module.exports = startServer