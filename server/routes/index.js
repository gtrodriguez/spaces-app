const express = require('express');
const path = require('path');
const assetsListData = require('../../mockdata/assetslist');
const entriesListData = require('../../mockdata/entrieslist');
const spacesListData = require('../../mockdata/spaceslist');
const usersListData = require('../../mockdata/userslist');
const router = express.Router();
const spaceRouter = express.Router({mergeParams: true});
const entriesRouter = express.Router({mergeParams: true});
const assetsRouter = express.Router({mergeParams: true});
const userRouter = express.Router({mergeParams: true});

router.get('/spaceexplorer*', function (req, res) {
  res.sendFile(path.join(__dirname + '/../views/index.html'));
});

router.use('/api/space', spaceRouter);
router.use('/api/users', userRouter);

spaceRouter.get('/',function(req, res, next) {
  res.status(200).json(spacesListData);
});

spaceRouter.get('/:space_id', function(req, res, next) {
  const result = spacesListData.items.filter(function(space){ return space.sys.id === req.params.space_id });

  if (result.length) {
    res.status(200).json(result);
  } else {
    res.status(404).json({
      message: "space not found"
    });
  }
});

spaceRouter.use('/:space_id/entries', entriesRouter);
spaceRouter.use('/:space_id/assets', assetsRouter);

assetsRouter.get('/', function(req, res, next) {
  const result = spacesListData.items.filter(function(space){ return space.sys.id === req.params.space_id });
  if (result.length) {
    const assets = assetsListData.items.filter(function(asset){ return asset.sys.space === req.params.space_id});
    res.status(200).json({
      "sys": {
        "type": "Array"
      },
      "total": 3,
      "skip": 0,
      "limit": 100,
      "items": assets});
  } else {
    res.status(404).json({
      message: "space not found"
    });
  }
});

assetsRouter.get('/:asset_id', function(req, res, next) {
  const result = spacesListData.items.filter(function(space){ return space.sys.id === req.params.space_id });
  if (result.length) {
    const asset = assetsListData.items.find(function(asset){ return asset.sys.space === req.params.space_id && asset.sys.id === req.params.asset_id});
    if (!asset) {
      res.status(404).json({
        message: "asset not found"
      });
    } else {
      res.status(200).json(asset)
    }
  } else {
    res.status(404).json({
      message: "space not found"
    });
  }
});

entriesRouter.get('/', function(req, res, next) {
  const result = spacesListData.items.filter(function(space){ return space.sys.id === req.params.space_id });
  
  if (result.length) {
    const entries = entriesListData.items.filter(function(entry){ return entry.sys.space === req.params.space_id});
    res.status(200).json({
      "sys": {
        "type": "Array"
      },
      "total": 3,
      "skip": 0,
      "limit": 100,
      "items": entries});
  } else {
    res.status(404).json({
      message: "space not found"
    });
  }
});

entriesRouter.get('/:entry_id', function(req, res, next) {
  const result = spacesListData.items.filter(function(space){ return space.sys.id === req.params.space_id });
  if (result.length) {
    const entry = entriesRouter.items.find(function(entry){ return entry.sys.space === req.params.space_id && entry.sys.id === req.params.entry_id});
    if (!entry) {
      res.status(404).json({
        message: "entry not found"
      });
    } else {
      res.status(200).json(entry)
    }
  } else {
    res.status(404).json({
      message: "space not found"
    });
  }
});

userRouter.get('/',function(req, res, next) {
  res.status(200).json({
    data: usersListData
  });
});

userRouter.get('/:user_id', function(req, res, next){
  const user = usersListData.items.find(function(user){ return user.sys.id === req.params.user_id});
  if (!user) {
    res.status(404).json({
      message: "user not found"
    });
  } else {
    res.status(200).json(user)
  }
});

module.exports = router;
